import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';

describe('App', () => {
  test('should render with ThemeProvider, Header, Footer and a Router containing Game and Highscore', () => {
    const wrapper =  shallow(<App />);
    expect(wrapper.find('ThemeProvider')).toHaveLength(1);
    expect(wrapper.find('Header')).toHaveLength(1);
    expect(wrapper.find('Footer')).toHaveLength(1);
    expect(wrapper.find('Router')).toHaveLength(1);
    expect(wrapper.find('Router Game')).toHaveLength(1);
    expect(wrapper.find('Router Highscores')).toHaveLength(1);
  });
});