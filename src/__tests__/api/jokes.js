import { getTenRandomJokes } from '../../api/jokes';

const fakeJokes =  [];
for (let i = 0; i < 10; i ++) fakeJokes.push({ test: 'test' });

afterEach(() => fetch.resetMocks());

describe('getTenRandomJokes', () => {
  test('should return array with jokes', () => {
    fetch.mockResponseOnce(JSON.stringify(fakeJokes));
    getTenRandomJokes().then(res => {
      expect(res).toEqual(fakeJokes);
    });
  });

  test('should return object with an error', () => {
    fetch.mockResponseOnce('', { status: 404 });
    getTenRandomJokes().then(res => {
      expect(res).toHaveProperty('error');
    });
  });
});