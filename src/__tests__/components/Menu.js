import React from 'react';
import { shallowWithTheme } from '../../testHelpers';
import Menu from '../../components/Menu';

describe('Menu', () => {
  test('should render a list with multiple children', () => {
    const wrapper = shallowWithTheme(<Menu>{['test', 'test', 'test']}</Menu>);
    expect(wrapper.find('li')).toHaveLength(3)
  });

  test('should render a list with only one child', () => {
    const wrapper = shallowWithTheme(<Menu>Test</Menu>);
    expect(wrapper.find('li')).toHaveLength(1)
  });
});