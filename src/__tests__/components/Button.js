import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import Button from '../../components/Button';

describe('Button', () => {
  test('should render with children and props', () => {
    const wrapper = mountWithTheme(<Button name="test">Test</Button>);
    expect(wrapper.containsMatchingElement(<Button name="test">Test</Button>)).toBe(true);
  });
});