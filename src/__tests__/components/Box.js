import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import Box from '../../components/Box';

describe('Box', () => {
  test('should render with children', () => {
    const wrapper = mountWithTheme(<Box><p>Test</p></Box>);
    expect(wrapper.containsMatchingElement(<Box><p>Test</p></Box>)).toBe(true);
  });
});