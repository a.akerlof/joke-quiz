import React from 'react';
import { mountWithTheme, shallowWithTheme } from '../../../testHelpers';
import Finish from '../../../components/game/Finish';
import { checkIfHighscore } from '../../../api/highscores';
jest.mock('../../../api/highscores');

describe('Finish', () => {
  test('should render game over with correct answers and total score', () => {
    checkIfHighscore.mockReturnValue(true);
    const wrapper = shallowWithTheme(<Finish correctAnswers={5} time={new Date(751000)} resetGame={() => {}} />)
    expect(wrapper.text().toLowerCase().search('game over')).not.toBe(-1);
    expect(wrapper.text().search('5')).not.toBe(-1);
    expect(wrapper.text().search('666')).not.toBe(-1);
  });

  test('should render with Highscore-component if game-session generated a highscore', () => {
    checkIfHighscore.mockReturnValue(true);
    const wrapper = shallowWithTheme(<Finish correctAnswers={1} time={new Date()} resetGame={() => {}} />);
    expect(checkIfHighscore).toHaveBeenCalled();
    expect(wrapper.find('Highscore')).toHaveLength(1);
  });

  test('should render with (reset)-Button if game-session didn\'t generate a highscore', () => {
    checkIfHighscore.mockReturnValue(false);
    const wrapper = mountWithTheme(<Finish correctAnswers={1} time={new Date()} resetGame={() => {}} />);
    expect(checkIfHighscore).toHaveBeenCalled();
    expect(wrapper.find('Button')).toHaveLength(1);
  });

  test('click on reset button should trigger a reset', () => {
    checkIfHighscore.mockReturnValue(false);
    const resetGame = jest.fn();
    const wrapper = mountWithTheme(<Finish correctAnswers={1} time={new Date()} resetGame={resetGame} />);
    wrapper.find('Button').simulate('click');
    expect(resetGame).toHaveBeenCalled();
  });
});