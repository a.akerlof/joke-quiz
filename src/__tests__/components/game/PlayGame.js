import React from 'react';
import { shallowWithTheme } from '../../../testHelpers';
import PlayGame from '../../../components/game/PlayGame';

describe('PlayGame', () => {
  let wrapper;
  beforeEach(() => wrapper = shallowWithTheme(<PlayGame jokes={[{}, {}]} resetGame={() => {}} />));
  
  test('should render with Timer and Joke', () => {
    expect(wrapper.find('Timer')).toHaveLength(1);
    expect(wrapper.find('Joke')).toHaveLength(1);
  });

  test('should increment activeJoke when incrementActiveJoke is called ', () => {
    wrapper.find('Joke').props().incrementActiveJoke();
    expect(wrapper.find('Joke').props().activeJoke).toBe(2)
  });
  
  test('should render with Finish after last joke', () => {
    wrapper.setProps({ jokes: []})
    expect(wrapper.find('Finish')).toHaveLength(1);
  });

  test('should increment correctAnswers when incrementCorrectAnswers is called', () => {
    wrapper.find('Joke').props().incrementCorrectAnswers();
    wrapper.setProps({ jokes: []});
    expect(wrapper.find('Finish').props().correctAnswers).toBe(1)
  });
  
  test('should set totalTime when setTotalTime is called', () => {
    const fakeTime = new Date(123);
    wrapper.find('Timer').props().setTotalTime(fakeTime);
    wrapper.setProps({ jokes: []});
    expect(wrapper.find('Finish').props().time).toBe(fakeTime)
  });
});