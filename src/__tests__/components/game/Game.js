import React from 'react';
import { shallowWithTheme } from '../../../testHelpers';
import Game from '../../../components/game/Game';

describe('Game', () => {
  const wrapper = shallowWithTheme(<Game />);

  test('should render the game with StartGame', () => {
    expect(wrapper.find('StartGame')).toHaveLength(1);
  });

  test('should render the game with PlayGame and pass jokes to PlayGame when game starts', () => {
    const fakeJokes = [{ joke: 'fake' }];
    wrapper.find('StartGame').props().setJokes(fakeJokes);
    wrapper.find('StartGame').props().setGameStarted(true)
    expect(wrapper.find('PlayGame')).toHaveLength(1);
    expect(wrapper.find('PlayGame').props().jokes).toBe(fakeJokes);
  });

  test('should render the game with StartGame when resetGame is called', () => {
    wrapper.props().children.props.resetGame();
    expect(wrapper.find('StartGame')).toHaveLength(1)
  });
});