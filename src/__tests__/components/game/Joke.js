import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import Joke from '../../../components/game/Joke';

describe('Joke', () => {
  const fakeJoke = { setup: 'test-joke-setup', punchline: 'test-joke-punchline' }
  
  let wrapper, incrementActiveJoke, incrementCorrectAnswers, setGameOver;
  beforeEach(() => {
    incrementActiveJoke = jest.fn();
    incrementCorrectAnswers = jest.fn();
    setGameOver = jest.fn();
    wrapper = mountWithTheme(
      <Joke 
        joke={fakeJoke}
        activeJoke={1}
        totalJokes={10}
        incrementActiveJoke={incrementActiveJoke}
        incrementCorrectAnswers={incrementCorrectAnswers}
        setGameOver={setGameOver}
      />
    );
  });

  test('should render with setup, "activeJoke / totalJokes" and a form with an answer input and a button', () => {
    expect(wrapper.text().search(fakeJoke.setup)).not.toBe(-1);
    expect(wrapper.text().search('1 / 10')).not.toBe(-1);
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('input[name="answer"]')).toHaveLength(1);
    expect(wrapper.find('button')).toHaveLength(1);
  });

  test('should trigger incrementCorrectAnswers and render with punchline and "correct answer" when form is submitted with correct answer', () => {
    wrapper.find('input[name="answer"]').simulate('change', { target: { value: fakeJoke.punchline }});
    wrapper.find('form').simulate('submit');
    expect(incrementCorrectAnswers).toHaveBeenCalled();
    expect(wrapper.text().search(fakeJoke.punchline)).not.toBe(-1);
    expect(wrapper.text().toLowerCase().search('correct answer')).not.toBe(-1);
  });

  test('should render with punchline and "wrong answer" when form is submitted with incorrect answer', () => {
    wrapper.find('input[name="answer"]').simulate('change', { target: { value: "wrong answer" }});
    wrapper.find('form').simulate('submit');
    expect(incrementCorrectAnswers).not.toHaveBeenCalled();
    expect(wrapper.text().search(fakeJoke.punchline)).not.toBe(-1);
    expect(wrapper.text().toLowerCase().search('wrong answer')).not.toBe(-1);
  });

  test('should render a next joke button after submission', () => {
    wrapper.find('form').simulate('submit');
    expect(wrapper.find('button').text().toLowerCase().search('next joke')).not.toBe(-1);
  });

  test('should render a finish button and trigger setGameOver after last answer is submitted', () => {
    wrapper.setProps({ activeJoke: 10 });
    wrapper.find('form').simulate('submit');
    expect(wrapper.find('button').text().toLowerCase().search('finish')).not.toBe(-1);
    expect(setGameOver).toHaveBeenCalled();
  });

  test('should trigger incrementActiveJoke and re-render with empty form on "next joke"-click, submission of new form should render wrong answer', () => {
    wrapper.find('input[name="answer"]').simulate('change', { target: { value: fakeJoke.punchline }});
    wrapper.find('form').simulate('submit');
    wrapper.find('button').simulate('click');
    expect(incrementActiveJoke).toHaveBeenCalled();
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('input[name="answer"]').props().value).toBe('');
    wrapper.find('form').simulate('submit');
    expect(wrapper.text().toLowerCase().search('wrong answer')).not.toBe(-1);
  });
});