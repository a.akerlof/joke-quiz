import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import { act } from '@testing-library/react';
import delay from 'delay';
import Timer from '../../../components/game/Timer';

describe('Timer', () => {
  const setTotalTime = jest.fn();
  const wrapper = mountWithTheme(<Timer run={true} setTotalTime={setTotalTime} />)

  test('should render a time element with a running timer', async() => {
    expect(wrapper.find('time').text()).toBe('00:00');
    await act(() => delay(1000));
    expect(wrapper.find('time').text()).toBe('00:01');
  });

  test('should stop timer and call setTotalTime when timer stops', async () => {
    wrapper.setProps({ run: false });
    const timerValue = wrapper.find('time').text();
    await act(() => delay(1000));
    expect(wrapper.find('time').text()).toBe(timerValue);
    expect(setTotalTime).toHaveBeenCalled();
  });
});