import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import Highscore from '../../../components/game/Highscore';
import { navigate } from '@reach/router';
import { setHighscore } from '../../../api/highscores';

jest.mock('@reach/router');
jest.mock('../../../api/highscores');

describe('Highscore', () => {
  const wrapper = mountWithTheme(<Highscore score={100} time={'00:00'} />);
  
  test('should render Highscore component with a form, a name input and a button', () => {
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('input[name="name"]')).toHaveLength(1);
    expect(wrapper.find('button')).toHaveLength(1);
  });

  test('should trigger a call to setHighscore with provided name, score and time, then navigate to /highscores when form is submitted', () => {
    wrapper.find('input[name="name"]').simulate('change', { target: { value: 'test' }})
    wrapper.find('form').simulate('submit');
    expect(setHighscore).toHaveBeenCalledWith('test', 100, '00:00');
    expect(navigate).toHaveBeenCalledWith('/highscores');
  });
});