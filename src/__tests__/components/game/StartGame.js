import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import StartGame from '../../../components/game/StartGame';
import { act } from '@testing-library/react';
import { getTenRandomJokes } from '../../../api/jokes';
jest.mock('../../../api/jokes');

describe('StartGame', () => {
  let wrapper;
  let setGameStarted, setJokes;
  beforeEach(() => {
    setGameStarted = jest.fn();
    setJokes = jest.fn();
    wrapper = mountWithTheme(<StartGame setGameStarted={setGameStarted} setJokes={setJokes} />)
  });
  
  test('should render with a start button', () => {
    expect(wrapper.find('button').text().toLowerCase()).toBe('start');
  });

  test('should render with Loader, then fetch jokes, set jokes and start game, on start-button click', async () => {
    getTenRandomJokes.mockResolvedValue('fake-jokes');
    wrapper.find('button').simulate('click');
    expect(wrapper.find('Loader')).toHaveLength(1);
    await act(async () => await Promise.resolve());
    expect(setGameStarted).toHaveBeenCalledWith(true);
    expect(setJokes).toHaveBeenCalledWith('fake-jokes');
  });

  test('should render a error message when fetching of jokes fail', async () => {
    getTenRandomJokes.mockResolvedValue({ error: 'fake-error-message'});
    wrapper.find('button').simulate('click');
    await act(async () => await Promise.resolve());
    expect(wrapper.text().search('fake-error-message')).not.toBe(-1);
  });
});