import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import Footer from '../../components/Footer';

describe('Footer', () => {
  test('should render a footer', () => {
    const wrapper = mountWithTheme(<Footer />);
    expect(wrapper.find('footer')).toHaveLength(1);
  });
});