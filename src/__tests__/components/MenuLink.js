import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import MenuLink from '../../components/MenuLink';

describe('MenuLink', () => {
  test('should render a link to /test', () => {
    const wrapper = mountWithTheme(<MenuLink to="/test">Test</MenuLink>);
    expect(wrapper.find('a').props().href).toBe('/test');
  });
});