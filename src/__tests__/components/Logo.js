import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import Logo from '../../components/Logo';

describe('Logo', () => {
  test('should render an h1 element', () => {
    const wrapper = mountWithTheme(<Logo />);
    expect(wrapper.find('h1')).toHaveLength(1);
  });
});