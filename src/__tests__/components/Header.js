import React from 'react';
import { mountWithTheme } from '../../testHelpers';
import Header from '../../components/Header';

describe('Header', () => {
  test('should render a header with Logo and Menu', () => {
    const wrapper = mountWithTheme(<Header />);
    expect(wrapper.find('header')).toHaveLength(1);
    expect(wrapper.find('Logo')).toHaveLength(1);
    expect(wrapper.find('Menu')).toHaveLength(1);
  });
});