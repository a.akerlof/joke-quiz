import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import HighscoreList from '../../../components/highscores/HighscoreList';

describe('HighscoreList', () => {
  const fakeHighscores = [
    { name: 'first', score: 300, time: '00:00' },
    { name: 'second', score: 200, time: '00:00' },
    { name: 'third', score: 100, time: '00:00' }
  ];

  test('should render a table with highscores', () => {
    const wrapper = mountWithTheme(<HighscoreList highscores={fakeHighscores} />);
    expect(wrapper.find('table')).toHaveLength(1);
    expect(wrapper.find('thead tr th')).toHaveLength(4);
    expect(wrapper.find('tbody tr td')).toHaveLength(12);
  });

  test('should render a div with a message longer then 5 characters if no highscores', () => {
    const wrapper = mountWithTheme(<HighscoreList highscores={[]} />);
    expect(wrapper.find('div').text().length).toBeGreaterThan(5);
  });
});