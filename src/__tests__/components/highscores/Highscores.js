import React from 'react';
import { mountWithTheme } from '../../../testHelpers';
import Highscores from '../../../components/highscores/Highscores';

describe('Highscores', () => {
  test('should render a heading and a HighscoreList', () => {
    const wrapper = mountWithTheme(<Highscores />);
    expect(wrapper.find('h2')).toHaveLength(1);
    expect(wrapper.find('HighscoreList')).toHaveLength(1);
  });
});