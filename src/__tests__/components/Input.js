import React from 'react'
import { mountWithTheme } from '../../testHelpers';
import Input from '../../components/Input';

describe('Input', () => {
  test('should render with props', () => {
    const wrapper = mountWithTheme(<Input name="test" />);
    expect(wrapper.containsMatchingElement(<Input name="test" />)).toBe(true);
  });
});