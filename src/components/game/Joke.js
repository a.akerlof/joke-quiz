/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import Input from '../Input';
import stripString from '../../utils/stripString';

const Joke = ({
  joke,
  activeJoke,
  totalJokes,
  incrementActiveJoke,
  incrementCorrectAnswers,
  setGameOver
}) => {
  const { setup, punchline } = joke;
  const [answer, setAnswer] = useState('');
  const [answered, setAnswered] = useState(false);
  const [correct, setCorrect] = useState(false);

  const handleChange = ({ target }) => setAnswer(target.value);
  const handleSubmit = e => {
    e.preventDefault();
    setAnswered(true);
    if (stripString(answer) === stripString(punchline)) {
      setCorrect(true);
      incrementCorrectAnswers();
    }
  }

  const nextJoke = () => {
    incrementActiveJoke();
    setAnswer('');
    setAnswered(false);
    setCorrect(false);
  }

  useEffect(() => { if (activeJoke === totalJokes && answered) setGameOver(true) });

  return (
    <div sx={{ textAlign: 'center', width: '100%' }}>
      <Styled.h2 sx={{ marginBottom: 0 }}>Joke</Styled.h2>
      <p
        data-test="active-joke"
        sx={{ fontFamily: 'heading', fontSize: 1, marginTop: 0 }}
      >
        {activeJoke} / {totalJokes}
      </p>
      <p 
        data-test="joke-setup"
        sx={{
          fontFamily: 'body',
          fontSize: 2,
        }
      }>
        {setup}
      </p>
      {
        !answered
          ?
          <form onSubmit={handleSubmit}>
            <Input
              data-test="joke-answer" 
              name="answer"
              value={answer}
              onChange={handleChange}
              autoFocus={true} 
              sx={{ width: ['100%', '75%'], margin: '0 auto', display: 'block' }}
            />
            <Button
              data-test="joke-guess"
              children="Guess Punchline"
              sx={{ marginTop: 3, marginX: 'auto', width: 'fit-content' }}
            />
          </form>
          :
          <div sx={{
            fontFamily: 'body',
            fontSize: 2,
          }}>
            <p 
              data-test="joke-punchline"
              sx={{ fontWeight: 'bold' }}
            >
              {punchline}
            </p>
            {
              correct
                ? <p data-test="correct-answer" sx={{ color: 'correct' }}>Correct answer!</p>
                : <p data-test="wrong-answer" sx={{ color: 'error' }}>Wrong answer</p>
            }
            <div sx={{ width: 'fit-content', margin: '0 auto' }}>
              <Button data-test="next-joke" onClick={nextJoke} autoFocus={true}>
                {activeJoke !== totalJokes ? 'Next Joke' : 'Finish!'}
              </Button>
            </div>
          </div>
      }
    </div>
  );
}

export default Joke

Joke.propTypes = {
  joke: PropTypes.object.isRequired,
  activeJoke: PropTypes.number.isRequired,
  totalJokes: PropTypes.number.isRequired,
  incrementActiveJoke: PropTypes.func.isRequired,
  incrementCorrectAnswers: PropTypes.func.isRequired,
  setGameOver: PropTypes.func.isRequired
}