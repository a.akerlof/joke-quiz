/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { navigate } from '@reach/router'
import Input from '../Input';
import Button from '../Button';
import { setHighscore } from '../../api/highscores';

const Highscore = ({ score, time }) => {
  const [name, setName] = useState('');
  const handleChange = ({ target }) => setName(target.value);
  const handleRegistration = e => {
    e.preventDefault();
    setHighscore(name, score, time);
    navigate('/highscores');
  }
  return (
    <div>
      <Styled.h2>That's a new Highscore</Styled.h2>
      <form onSubmit={handleRegistration}>
        <label sx={{ fontFamily: 'body', fontSize: 2, textAlign: 'center'}}>
          Enter your name:
          <Input 
            data-test="highscore-name"
            autoFocus={true}
            type="text"
            name="name"
            value={name}
            onChange={handleChange}
            maxLength="16"
            sx={{ width: ['75%', '50%'], marginTop: 3, marginX: 'auto', display: 'block' }} 
          />
        </label>
        <Button
          data-test="highscore-reg"
          children="Register"
          sx={{ marginTop: 3, marginX: 'auto', width: 'fit-content' }}
        />
      </form>
    </div>
  );
}

export default Highscore;

Highscore.propTypes = {
  score: PropTypes.number.isRequired,
  time: PropTypes.string.isRequired
}