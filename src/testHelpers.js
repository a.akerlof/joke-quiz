import React from 'react';
import { ThemeProvider } from 'theme-ui';
import { theme } from './theme';
import { mount, shallow } from 'enzyme';

export const mountWithTheme = (child) => mount(child, {
  wrappingComponent: ({ children }) => 
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
});

export const shallowWithTheme = (child) => shallow(child, {
  wrappingComponent: ({ children }) => 
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
});