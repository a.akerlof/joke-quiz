export const API_URL = 'https://official-joke-api.appspot.com';

export const getTenRandomJokes = () => {
  const url = `${API_URL}/jokes/ten`;
  return fetch(url)
    .then(res => {
      if (res.status === 200) return res.json();
      else throw new Error();
    })
    .then(data => data)
    .catch(() => ({ error: 'Something went wrong, please try again!' }));
}