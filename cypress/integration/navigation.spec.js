describe('Navigation', () => {
  it('visits my localhost and renders app', () => {
    cy.visit('/');

    cy.get('h1')
      .invoke('text')
      .should('match', /joke quiz/i);
  });

  it('navigates to highscores', () => {
    cy.get('nav a[href="/highscores"]')
      .click();

    cy.location('pathname')
      .should('equal', '/highscores');

    cy.get('h2')
      .invoke('text')
      .should('match', /highscores/i);
  });

  it('navigates to game', () => {
    cy.get('nav a[href="/"]')
      .click();

    cy.location('pathname')
      .should('equal', '/');

    cy.get('h2')
      .invoke('text')
      .should('match', /play a game/i);
  });
});