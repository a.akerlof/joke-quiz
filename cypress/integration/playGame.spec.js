const API_URL = require('../../src/api/jokes').API_URL;

describe('Play a full game', () => {
  let apiResponse;

  it('visits localhost, starts game (triggers api call), shows loader while resolving call', () => {
    cy.visit('/', { onBeforeLoad: win => delete win.fetch });
    
    cy.server();
    cy.route('GET', `${API_URL}/jokes/ten`).as('fetchJokes');

    cy.get('[data-test="start-button"]').click();
    cy.get('[data-test="loader"]').should('exist');
    
    cy.wait('@fetchJokes')
      .then(xhr => xhr.response.body.text())
      .then(json => apiResponse = JSON.parse(json));
  });
 
  it('runs through all questions providing right and wrong answers', () => {
    apiResponse.forEach((joke, i) => {
      cy.get('[data-test="active-joke"]').should('have.text', `${i+1} / ${apiResponse.length}`);
      cy.get('[data-test="joke-setup"]').should('have.text', apiResponse[i].setup)

      const answer = i % 2 ? apiResponse[i].punchline : 't35t1ng wr0ng 4n5w3r';
      
      cy.get('[data-test="joke-answer"]').type(answer);
      cy.get('[data-test="joke-guess"]').click();
      cy.get('[data-test="joke-punchline"]').should('have.text', apiResponse[i].punchline);
      
      i % 2
        ? cy.get('[data-test="correct-answer"]').should('exist')
        : cy.get('[data-test="wrong-answer"]').should('exist');

      cy.get('[data-test="next-joke"]').click();      
    });
  });

  it('should should show game over screen when game is finished', () => {
    cy.get('[data-test="game-over"]').should('exist');
    cy.get('[data-test="correct-answers').should('have.text', `${Math.ceil(apiResponse.length / 2)}`);
    cy.get('[data-test="total-score"]').should('not.be.empty');
  });

  it('should register a highscore', () => {
    cy.get('[data-test="highscore-name"]').type('TestPlayer');
    cy.get('[data-test="highscore-reg"]').click();
    cy.location('pathname').should('equal', '/highscores');
    cy.wait(1000);
    cy.get('[data-test="highscores"]').contains('TestPlayer').should('exist');
  })

});