const API_URL = require('../../src/api/jokes').API_URL;

describe('Failing to play a game', () => {
  it('visits localhost, starts game, failing api call', () => {
    cy.visit('/', { onBeforeLoad: win => delete win.fetch });

    cy.server();
    cy.route({
      method: 'GET', 
      url: `${API_URL}/jokes/ten`,
      status: 404,
      response: ''
    }).as('fetchJokes');

    cy.get('[data-test="start-button"]').click();
    cy.wait('@fetchJokes')
  });

  it('shows an error message', () => {
    cy.get('[data-test="error-msg"]')
      .should('exist');
  });
});